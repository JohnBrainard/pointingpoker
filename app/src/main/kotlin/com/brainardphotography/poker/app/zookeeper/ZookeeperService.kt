package com.brainardphotography.poker.app.zookeeper

import com.brainardphotography.poker.PokerUser
import com.brainardphotography.utility.kotlin.toByteArray
import com.brainardphotography.utility.kotlin.toObject
import org.apache.zookeeper.*
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

typealias OnUsersChanged = (users: List<PokerUser>) -> Unit

class ZookeeperService : Runnable {
	private var zk: ZooKeeper? = null
	private var thread: Thread? = null

	private val running: AtomicBoolean = AtomicBoolean(false)

	// events
	private var onUsersChanged: OnUsersChanged? = null

	val isRunning: Boolean
		get() = running.get()

	fun start() {
		thread = Thread(this, "Zookeeper Service Thread")
		thread?.start()
	}

	fun login(name: String, id: UUID) {
		val login = PokerUser(id, name)

		try {
			zk?.create("/poker/users/$id", login.toByteArray(), ZooDefs.Ids.READ_ACL_UNSAFE, CreateMode.EPHEMERAL)
		} catch (ex: KeeperException) {
			ex.printStackTrace()
		}
	}

	fun shutdown() {
		zk?.close()
		running.set(false)
	}

	override fun run() {
		val watcher = Watcher {
			println("Something happened!")
		}

		zk = ZooKeeper("solr0:2181", 3000, watcher).apply {
			val poker = exists("/poker", watcher)
			if (poker == null) {
				create("/poker", null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
			}

			val users = exists("/poker/users") { println("users exists...") }
			if (users == null) {
				create("/poker/users", null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
			}
		}

		watchChildren()

		running.set(true)
	}

	fun onUsersChanged(fn: OnUsersChanged) {
		this.onUsersChanged = fn
	}

	private fun watchChildren() {
		zk?.getChildren("/poker/users") { event ->
			fireOnUsersChanged(event)
		}
	}

	private fun fireOnUsersChanged(event: WatchedEvent) {
		val children = zk?.getChildren("/poker/users", false)
		val users = children?.map {
			zk?.getData("/poker/users/$it", false, null)?.toObject() as PokerUser
		}

		if (users != null) {
			onUsersChanged?.invoke(users)
		}

		watchChildren()
	}
}
