package com.brainardphotography.poker.app

import tornadofx.View
import tornadofx.hbox
import tornadofx.label
import tornadofx.plusAssign

class StatusBarView : View() {
	override val root = hbox {
		this += label("Starting Zookeeper")
	}
}