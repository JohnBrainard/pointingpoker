package com.brainardphotography.poker.app

import tornadofx.*

class MenuBarView : View() {
	val controller: MenuBarController by inject()

	override val root = menubar {
		menu("File") {
			menuitem("Join Session") {
				controller.login()
			}

			menuitem("Quit", "Shortcut+Q") {
				controller.quit()
			}
		}
	}
}