package com.brainardphotography.poker.app

import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleBooleanProperty
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority.ALWAYS
import tornadofx.*

class LoginView : View("Login") {
	val controller: LoginController by inject()

	override val savable = SimpleBooleanProperty(false)
	override val refreshable = SimpleBooleanProperty(false)

	val onLogin = EventHandler<ActionEvent> {
		if (controller.name.isNotEmpty()) {
			runAsync {
				controller.login()
			} ui {
				workspace.dock<JoinSessionView>(scope = this.scope)
			}
		}
	}

	override fun onUndock() {
		workspace.viewStack.remove(this)
	}

	override val root = HBox(8.0)

	init {
		with(root) {
			paddingAll = 16
			alignment = Pos.BASELINE_LEFT

			label("Name")
			textfield(controller.nameProperty) {
				hgrow = ALWAYS
				onAction = onLogin
			}
			button("Connect") {
				onAction = onLogin
				enableWhen { Bindings.isNotEmpty(controller.nameProperty) }
			}
		}
	}
}
