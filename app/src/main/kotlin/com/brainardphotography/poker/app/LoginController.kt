package com.brainardphotography.poker.app

import com.brainardphotography.poker.PokerSession
import com.brainardphotography.poker.PokerSessionEntry
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import java.util.*

class LoginController : Controller() {
	override val scope = super.scope as PokerScope

	val nameProperty = SimpleStringProperty("")
	val name by nameProperty

	val sessionNameProperty = SimpleStringProperty()
	val sessionName by sessionNameProperty

	val selectedSessionProperty = SimpleObjectProperty<PokerSession>()
	val selectedSession: PokerSession by selectedSessionProperty

	val observingProperty = SimpleBooleanProperty(false)
	val observing by observingProperty

	fun login() {
		scope.poker.login(scope.userId, nameProperty.get())
	}

	fun joinSession() {
		val entry = PokerSessionEntry(scope.userId, observing, null)
		scope.joinSession(selectedSession, entry)
	}

	fun createSession() {
		val session = PokerSession(UUID.randomUUID(), sessionNameProperty.value)
		scope.poker.createSession(session)
	}

	fun showVotes() {
		scope.activeSession.displayVotes = true
		scope.poker.updateSession(scope.activeSession)
	}
}