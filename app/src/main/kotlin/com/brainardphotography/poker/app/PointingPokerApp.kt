package com.brainardphotography.poker.app

import javafx.stage.Stage
import tornadofx.*
import java.util.*

class PointingPokerApp : App(Workspace::class) {
	val userId: UUID = UUID.randomUUID()

	override fun start(stage: Stage) {
		this.scope = PokerScope()

		this.workspace.title = "Pointing Poker!"

		stage.width = 800.0
		stage.height = 600.0

		val scope = this.scope as PokerScope
		scope.poker.connect("solr0:2181")

		super.start(stage)
	}

	override fun stop() {
		val scope = this.scope as PokerScope
		scope.poker.disconnect()
	}

	override fun onBeforeShow(view: UIComponent) {
		workspace.dock<LoginView>(scope = this.scope)
	}
}