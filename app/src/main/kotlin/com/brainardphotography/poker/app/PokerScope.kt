package com.brainardphotography.poker.app

import com.brainardphotography.poker.PointingPoker
import com.brainardphotography.poker.PokerSession
import com.brainardphotography.poker.PokerSessionEntry
import com.brainardphotography.poker.PokerUser
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import tornadofx.*
import java.util.*

class PokerScope : Scope() {
	val userId: UUID = UUID.randomUUID()
	val poker = PointingPoker()

	val sessions: ObservableList<PokerSession> = FXCollections.observableArrayList()
	val users: ObservableList<PokerUser> = FXCollections.observableArrayList()

	val activeSessionProperty: SimpleObjectProperty<PokerSession> = SimpleObjectProperty()
	val activeSession by activeSessionProperty

	val displayVotesProperty = SimpleBooleanProperty(false)
	val displayVotes by displayVotesProperty

	val sessionEntriesProperty: ObservableList<PokerSessionEntry> = FXCollections.observableArrayList()

	init {
		poker.onSessionsChanged { sessions ->
			updateSessions(sessions)
		}

		poker.onUsersChanged { users ->
			updateUsers(users)
		}

		poker.onSessionChanged { session ->
			println("Session: ${session.displayVotes}")
			activeSessionProperty.value = session
			displayVotesProperty.value = session.displayVotes
		}

		poker.onSessionEntriesChanged { sessionEntries ->
			updateSessionEntries(sessionEntries)
		}

		displayVotesProperty.onChange { value ->
			if (value != activeSession.displayVotes) {
				activeSession.displayVotes = value
				poker.updateSession(activeSession)
			}
		}
	}

	fun joinSession(session: PokerSession, entry: PokerSessionEntry) {
		activeSessionProperty.value = session

		poker.watchSession(session)
		poker.watchSessionEntries(session)

		poker.addSessionEntry(session, entry)
	}

	fun showVotes(value: Boolean) {
		activeSession.displayVotes = value
		poker.updateSession(activeSession)
	}

	fun findUser(userId: UUID) = users.find { it.id == userId }

	fun refreshSessions() {
		task {
			poker.getSessions()
		} success { sessions ->
			updateSessions(sessions)
		}
	}

	private fun updateSessions(sessions: List<PokerSession>) {
		Platform.runLater {
			val remove = this.sessions.filter { !sessions.contains(it) }
			this.sessions.removeAll(remove)
			this.sessions.setAll(sessions)
		}
	}

	private fun updateUsers(users: List<PokerUser>) {
		Platform.runLater {
			val remove = this.users.filter { !users.contains(it) }
			this.users.removeAll(remove)
			this.users.setAll(users)
		}
	}

	private fun updateSessionEntries(entries: List<PokerSessionEntry>) {
		Platform.runLater {
			val remove = sessionEntriesProperty.filter { !entries.contains(it) }
			sessionEntriesProperty.removeAll(remove)
			sessionEntriesProperty.setAll(entries)
		}
	}
}