package com.brainardphotography.poker.app

import javafx.application.Platform
import tornadofx.Controller

class MenuBarController : Controller() {
	fun quit() {
		Platform.exit()
	}

	fun login() {
		val loginFragment = find<LoginView>()
		loginFragment.openModal(escapeClosesWindow = true)
		loginFragment.modalStage?.width = 400.0
		loginFragment.modalStage?.height = 400.0
	}
}