package com.brainardphotography.poker.app

import com.brainardphotography.poker.PokerSession
import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleBooleanProperty
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.control.ListView
import javafx.scene.control.SelectionMode
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority.ALWAYS
import tornadofx.*

class JoinSessionView : View("Join session") {
	override val scope = super.scope as PokerScope

	val controller: LoginController by inject()

	override val root = BorderPane()
	override val savable = SimpleBooleanProperty(false)

	private var sessonsList: ListView<PokerSession>? = null

	override fun onRefresh() {
		scope.refreshSessions()
	}

	override fun onDock() {
		scope.refreshSessions()
	}

	override fun onGoto(source: UIComponent) {
		when (source) {
			this -> workspace.viewStack.remove(this)
			LoginView::class -> workspace.viewStack.remove(this)
		}
	}

	val onJoinSession = EventHandler<ActionEvent> { event ->
		runAsync {
			controller.joinSession()
		} ui {
			workspace.dock<SessionView>(scope = this.scope)
		}
	}

	val onCreateSession = EventHandler<ActionEvent> { event ->
		runAsync {
			controller.createSession()
		} ui {
			controller.sessionNameProperty.set("")
		}
	}

	init {
		with(root) {
			paddingAll = 8

			top {
				hbox(8) {
					paddingAll = 8
					alignment = Pos.BASELINE_LEFT

					label("Session Name")
					textfield(controller.sessionNameProperty) {
						hgrow = ALWAYS
						onAction = onCreateSession

						requestFocus()
					}
					button("Create") {
						onAction = onCreateSession
					}
				}
			}

			center {
				vbox {
					paddingAll = 8

					sessonsList = listview(scope.sessions) {
						selectionModel.selectionMode = SelectionMode.SINGLE

						cellCache {
							label(it.name)
						}

						bindSelected(controller.selectedSessionProperty)
						onUserSelect(2) { onJoinSession.handle(null) }
					}
				}
			}

			bottom {
				borderpane {
					paddingAll = 8.0

					left {
						checkbox("Observing?", controller.observingProperty)
					}

					right {
						button("Join session") {
							onAction = onJoinSession

							enableWhen {
								Bindings.isNotEmpty(sessonsList?.selectionModel?.selectedItems!!)
							}
						}
					}
				}
			}
		}
	}
}
