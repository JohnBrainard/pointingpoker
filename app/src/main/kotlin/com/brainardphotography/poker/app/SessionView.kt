package com.brainardphotography.poker.app

import com.brainardphotography.poker.PokerPointValue
import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.control.SelectionMode
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import tornadofx.*

class SessionView : View() {
	override val scope: PokerScope = super.scope as PokerScope
	override val savable = SimpleBooleanProperty(false)

	//	override val scope = super.scope as PokerScope
	override val root = BorderPane()

	init {
		with(root) {
			paddingAll = 8

			center {
				vbox {
					paddingAll = 8
					listview(scope.sessionEntriesProperty.filtered { !it.observing }) {
						vgrow = Priority.ALWAYS
						selectionModel.selectionMode = SelectionMode.SINGLE

						cellCache {
							borderpane {
								paddingAll = 8.0

								left {
									label(scope.findUser(it.userId)?.name ?: "<no name>")
								}

								right {
									label("Vote!") {
										visibleWhen { scope.displayVotesProperty }
									}
								}
							}
						}
					}

					hbox(4) {
						paddingAll = 8
						togglegroup {
							PokerPointValue.values().forEach {
								togglebutton(it.display) {

								}
							}
						}
					}
				}
			}
		}
	}

	override fun onDock() {
		title = "In ${scope.activeSession.name} session"
		heading = ""

		with(workspace) {
			checkbox("Show votes", this@SessionView.scope.displayVotesProperty)
		}
	}

	override fun onUndock() {
		scope.poker.removeSessionEntry(scope.activeSession, scope.userId)
		workspace.viewStack.remove(this)
	}
}