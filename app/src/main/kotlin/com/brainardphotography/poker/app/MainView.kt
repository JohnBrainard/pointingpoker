package com.brainardphotography.poker.app

import javafx.scene.control.Label
import tornadofx.View
import tornadofx.borderpane

class MainView : View("Pointing Poker!") {
	override val root = borderpane {
		top(MenuBarView::class)
		bottom(StatusBarView::class)
		center = Label("Pointing Poker!")
	}
}