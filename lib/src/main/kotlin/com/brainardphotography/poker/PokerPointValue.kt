package com.brainardphotography.poker

enum class PokerPointValue(val display: String, val value: Double) {
	//0, ½, 1, 2, 3, 5, 8, 13, 20, 40, 100

	ZERO("0", 0.0),
	ONE_HALF("½", 0.5),
	TWO("2", 2.0),
	THREE("3", 3.0),
	FIVE("5", 5.0),
	EIGHT("8", 8.0),
	THIRTEEN("13", 13.0),
	TWENTY("20", 20.0),
	FORTY("40", 40.0),
	ONE_HUNDRED("100", 100.0)
}