package com.brainardphotography.poker

import java.io.Serializable
import java.util.*

class PokerSession(val id: UUID, val name: String, var displayVotes: Boolean = false) : Serializable