package com.brainardphotography.poker

import com.brainardphotography.utility.kotlin.toByteArray
import com.brainardphotography.utility.kotlin.toObject
import org.apache.zookeeper.*
import java.util.*

typealias OnUsersChanged = (user: List<PokerUser>) -> Unit
typealias OnSessionsChanged = (sessions: List<PokerSession>) -> Unit
typealias OnSessionChanged = (session: PokerSession) -> Unit
typealias OnSessionEntriesChanged = (entries: List<PokerSessionEntry>) -> Unit

const val ZK_PATH = "/poker"

class PointingPoker {

	private var zookeeper: ZooKeeper? = null
	private var onUsersChanged: OnUsersChanged? = null
	private var onSessionsChanged: OnSessionsChanged? = null
	private var onSessionChanged: OnSessionChanged? = null
	private var onSessionEntriesChanged: OnSessionEntriesChanged? = null

	private var watchedSession: PokerSession? = null

	fun connect(zkHost: String) {
		zookeeper?.close()
		zookeeper = ZooKeeper(zkHost, 3000) { event -> processEvent(event) }

		init()
		watchUsers()
		watchSessions()
	}

	fun disconnect() {
		zookeeper?.close()
	}

	fun init() {
		zookeeper?.apply {
			if (exists(ZK_PATH, false) == null) {
				create(ZK_PATH, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
			}

			if (exists("$ZK_PATH/sessions", false) == null) {
				create("$ZK_PATH/sessions", null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
			}

			if (exists("$ZK_PATH/users", false) == null) {
				create("$ZK_PATH/users", null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
			}
		}
	}

	fun login(id: UUID, name: String) {
		val user = PokerUser(id, name)

		try {
			zookeeper?.create("/poker/users/$id",
					user.toByteArray(),
					ZooDefs.Ids.READ_ACL_UNSAFE,
					CreateMode.EPHEMERAL)
		} catch (ex: KeeperException) {
			ex.printStackTrace()
		}
	}

	fun createSession(session: PokerSession) {
		try {
			val path = zookeeper?.create("$ZK_PATH/sessions/${session.id}", session.toByteArray(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
			println("Created session at $path")
		} catch (ex: KeeperException) {
			ex.printStackTrace()
		}
	}

	fun updateSession(session: PokerSession) {
		val path = "$ZK_PATH/sessions/${session.id}"
		zookeeper?.setData(path, session.toByteArray(), -1)
	}

	fun getUsers(): List<PokerUser> {
		return zookeeper?.getChildren("$ZK_PATH/users", false)?.map { child ->
			zookeeper?.getData("$ZK_PATH/users/$child", false, null)?.toObject() as PokerUser
		} ?: emptyList()
	}

	fun getSessions(): List<PokerSession> {
		return zookeeper?.getChildren("$ZK_PATH/sessions", false)?.map { child ->
			zookeeper?.getData("$ZK_PATH/sessions/$child", false, null)?.toObject() as PokerSession
		} ?: emptyList()
	}

	fun getSession(path: String): PokerSession? {
		return zookeeper?.getData(path, false, null)?.toObject() as PokerSession
	}

	fun getSessionEntries(session: PokerSession): List<PokerSessionEntry> {
		val path = "$ZK_PATH/sessions/${session.id}"
		return zookeeper?.getChildren(path, false)?.map { entry ->
			zookeeper?.getData("$path/$entry", false, null)?.toObject() as PokerSessionEntry
		} ?: emptyList()
	}

	fun addSessionEntry(session: PokerSession, sessionEntry: PokerSessionEntry) {
		val path = "$ZK_PATH/sessions/${session.id}/${sessionEntry.userId}"

		val exists = zookeeper?.exists(path, false)
		when (exists) {
			null -> zookeeper?.create(path, sessionEntry.toByteArray(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL)
			else -> zookeeper?.setData(path, sessionEntry.toByteArray(), -1)
		}
	}

	fun removeSessionEntry(session: PokerSession, userId: UUID) {
		val path = "$ZK_PATH/sessions/${session.id}/${userId}"

		val exists = zookeeper?.exists(path, false)
		if (exists != null) {
			zookeeper?.delete(path, -1)
		}
	}

	fun onUsersChanged(listener: OnUsersChanged) {
		onUsersChanged = listener
	}

	internal fun fireOnUsersChanged(users: List<PokerUser>) {
		onUsersChanged?.invoke(users)
	}

	internal fun watchUsers() {
		zookeeper?.getChildren("$ZK_PATH/users") { event ->
			fireOnUsersChanged(getUsers())
			watchUsers()
		}
	}

	fun onSessionsChanged(listener: OnSessionsChanged) {
		onSessionsChanged = listener
	}

	internal fun fireOnSessionsChanged(sessions: List<PokerSession>) {
		onSessionsChanged?.invoke(sessions)
	}

	internal fun watchSessions() {
		zookeeper?.getChildren("$ZK_PATH/sessions") { event ->
			fireOnSessionsChanged(getSessions())
			watchSessions()
		}
	}

	fun onSessionChanged(listener: OnSessionChanged) {
		onSessionChanged = listener
	}

	internal fun fireOnSessionChanged(session: PokerSession) {
		onSessionChanged?.invoke(session)
	}

	fun watchSession(session: PokerSession) {
		watchedSession = session

		val watcher = Watcher { event ->
			val path = watchedSession?.let { "$ZK_PATH/sessions/${it.id}" }

			if (event.path == path) {
				val session = getSession(event.path)
				session?.apply {
					fireOnSessionChanged(this)
					watchSession(this)
				}
			}
		}

		val path = "$ZK_PATH/sessions/${session.id}"
		zookeeper?.getData(path, watcher, null)
	}

	fun onSessionEntriesChanged(listener: OnSessionEntriesChanged) {
		onSessionEntriesChanged = listener
	}

	internal fun fireOnSessionEntriesChanged(entries: List<PokerSessionEntry>) {
		onSessionEntriesChanged?.invoke(entries)
	}

	fun watchSessionEntries(session: PokerSession) {
		watchedSession = session

		val watcher = Watcher { event ->
			val path = watchedSession?.let { "$ZK_PATH/sessions/${it.id}" }

			if (event.path == path) {
				val session = getSession(event.path)!!
				val entries = getSessionEntries(session)

				entries?.apply {
					fireOnSessionEntriesChanged(entries)
					watchSessionEntries(session)
				}
			}
		}

		val path = "$ZK_PATH/sessions/${session.id}"
		zookeeper?.getChildren(path, watcher)
	}

	private fun processEvent(event: WatchedEvent) {

	}
}