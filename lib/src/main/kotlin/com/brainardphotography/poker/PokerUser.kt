package com.brainardphotography.poker

import java.io.Serializable
import java.util.*

data class PokerUser(val id: UUID, val name: String) : Serializable