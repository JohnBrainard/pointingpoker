package com.brainardphotography.poker

import java.io.Serializable
import java.util.*

data class PokerSessionEntry(val userId: UUID, val observing: Boolean, val vote: PokerPointValue?) : Serializable