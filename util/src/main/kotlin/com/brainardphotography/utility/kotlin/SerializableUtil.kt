package com.brainardphotography.utility.kotlin

import java.io.*

fun Serializable.toByteArray(): ByteArray {
	return ByteArrayOutputStream().use { bout ->
		ObjectOutputStream(bout).use { oout ->
			oout.writeObject(this)
		}

		bout.toByteArray()
	}
}

fun ByteArray.toObject(): Any {
	return ByteArrayInputStream(this).use { bin ->
		ObjectInputStream(bin).use(ObjectInputStream::readObject)
	}
}